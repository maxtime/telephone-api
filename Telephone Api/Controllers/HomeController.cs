﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Data;
using Telephone_Api.Models;
using System.Runtime.InteropServices;
using maxtime_Push_API.DLLs;
using System.IO;


namespace Telephone_Api.Controllers
{
    public class HomeController : ApiController
    {
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate int MAXTIME_Calc(String Server, int Port, String LoginName, String Password, String Database, int Employee, int Date, out StringBuilder ErrorString);


        [HttpPost]
        public HttpResponseMessage get_todays_schedules([FromBody]String Employee_Code)
        {
            HttpStatusCode hsc = new HttpStatusCode();
            IEnumerable<String> s = Request.Headers.GetValues("session_token");
            IEnumerable<String> app_name = !Request.Headers.Any(ob => ob.Key == "app_name") || Request.Headers.GetValues("app_name") == null ? null : Request.Headers.GetValues("app_name");
            IEnumerable<String> app_major = !Request.Headers.Any(ob => ob.Key == "app_major") || Request.Headers.GetValues("app_major") == null ? null : Request.Headers.GetValues("app_major");
            IEnumerable<String> app_minor = !Request.Headers.Any(ob => ob.Key == "app_minor") || Request.Headers.GetValues("app_minor") == null ? null : Request.Headers.GetValues("app_minor");

            try
            {
                ConnectionRepository cr = new ConnectionRepository();
                Dictionary<String, Object> prms = new Dictionary<String, Object>();

                using (HttpClient hc = new HttpClient())
                {
                    //No telelphone Number provided
                    if (Employee_Code == null || Employee_Code == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No employee Provided");
                    prms.Add("employee", Employee_Code);

                    //No session_token provided, return error response.
                    if (s == null || s.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Session Token Provided");
                    if (app_name == null || app_name.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Name Provided");
                    if (app_major == null || app_major.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Major Version Provided");
                    if (app_minor == null || app_minor.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Minor Version Provided");

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"session/check_valid_new");
                    req.Headers.Add("session_token", s.First());
                    req.Headers.Add("app_name", app_name.First());
                    req.Headers.Add("app_major", app_major.First());
                    req.Headers.Add("app_minor", app_minor.First());
                    req.Headers.Add("api_name", ConnectionRepository.API_name);
                    req.Headers.Add("api_major", ConnectionRepository.API_major);
                    req.Headers.Add("api_minor", ConnectionRepository.API_minor);

                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to check valid session for get_todays_schedules", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, Boolean>> b = JsonConvert.DeserializeObject<List<Dictionary<String, Boolean>>>(result.Content.ReadAsStringAsync().Result);

                    //Invalid, return error response.
                    if (b == null || b[0].First().Value == false) return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Session Token Provided");

                    req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_connection");
                    req.Headers.Add("session_token", s.First());

                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to get the connection for get_todays_schedules", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    String connection = cr.get_connection(d);
                    MySqlConnection msc = new MySqlConnection();
                    if (connection == "Invalid Connection")
                    {
                        throw new Exception("Failed to get the connection for get_todays_schedules", new Exception(connection));
                    }
                    else
                    {
                        msc = new MySqlConnection(connection);
                    }

                    List<Object> qry = cr.createQuery(msc, @"SELECT 
                                                                da.StartTime, 
                                                                da.EndTime, 
                                                                da.Notes,
                                                                o.Descr AS SiteDescr,
                                                                j.Descr 
                                                            FROM
                                                                day_assignments AS da
                                                            LEFT JOIN 
                                                                jobroles AS j ON j.Code = da.Jobrole_Code
                                                            LEFT JOIN 
                                                                organiz AS o ON o.Code = da.Site_Code
                                                            WHERE 
                                                                da.AssignDate = DATE(now()) AND da.employee = @employee ORDER BY da.StartTime", prms);
                    HttpResponseMessage resp = Request.CreateResponse((HttpStatusCode)qry[0], qry[1]);
                    return resp;
                }
            }
            catch (Exception e)
            {
                //Action failure, attempt to send email
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return Request.CreateResponse(HttpStatusCode.BadRequest, errorMessages);
                    Dictionary<String, Object> request = new Dictionary<String, Object>();
                    request.Add("Source", "Telephone API");
                    request.Add("Request Uri", Request.RequestUri);
                    request.Add("Methods", Request.Method);
                    request.Add("Headers", Request.Headers);
                    request.Add("Content", Request.Content);
                    request.Add("Parameters", new List<object>() { s, Employee_Code });
                    maxtime_functions.sendEmail("get_todays_schedules failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(request));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessages);
            }
        }
        
        [HttpPost]
        public HttpResponseMessage get_telephone_details([FromBody]String TelephoneNumber)
        {
            HttpStatusCode hsc = new HttpStatusCode();
            IEnumerable<String> s = Request.Headers.GetValues("session_token");
            IEnumerable<String> app_name = !Request.Headers.Any(ob => ob.Key == "app_name") || Request.Headers.GetValues("app_name") == null ? null : Request.Headers.GetValues("app_name");
            IEnumerable<String> app_major = !Request.Headers.Any(ob => ob.Key == "app_major") || Request.Headers.GetValues("app_major") == null ? null : Request.Headers.GetValues("app_major");
            IEnumerable<String> app_minor = !Request.Headers.Any(ob => ob.Key == "app_minor") || Request.Headers.GetValues("app_minor") == null ? null : Request.Headers.GetValues("app_minor");

            try
            {
                ConnectionRepository cr = new ConnectionRepository();
                Dictionary<String, Object> prms = new Dictionary<String, Object>();

                using (HttpClient hc = new HttpClient())
                {
                    //No telelphone Number provided
                    if (TelephoneNumber == null || TelephoneNumber == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Serial Number Provided");
                    prms.Add("telephone", TelephoneNumber);

                    //No session_token provided, return error response.
                    if (s == null || s.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Session Token Provided");
                    if (app_name == null || app_name.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Name Provided");
                    if (app_major == null || app_major.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Major Version Provided");
                    if (app_minor == null || app_minor.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Minor Version Provided");

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"session/check_valid_new");
                    req.Headers.Add("session_token", s.First());
                    req.Headers.Add("app_name", app_name.First());
                    req.Headers.Add("app_major", app_major.First());
                    req.Headers.Add("app_minor", app_minor.First());
                    req.Headers.Add("api_name", ConnectionRepository.API_name);
                    req.Headers.Add("api_major", ConnectionRepository.API_major);
                    req.Headers.Add("api_minor", ConnectionRepository.API_minor);

                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to check valid session for get_telephone_details", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, Boolean>> b = JsonConvert.DeserializeObject<List<Dictionary<String, Boolean>>>(result.Content.ReadAsStringAsync().Result);

                    //Invalid, return error response.
                    if (b == null || b[0].First().Value == false) return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Session Token Provided");

                    req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_connection");
                    req.Headers.Add("session_token", s.First());

                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to get the connection for get_telephone_details", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    String connection = cr.get_connection(d);
                    MySqlConnection msc = new MySqlConnection();
                    if (connection == "Invalid Connection")
                    {
                        throw new Exception("Failed to get the connection for get_telephone_details", new Exception(connection));
                    }
                    else
                    {
                        msc = new MySqlConnection(connection);
                    }

                    List<Object> qry = cr.createQuery(msc, @"select Code from organiz WHERE tel = @telephone AND CanTelClockIn = 1;", prms);
                    HttpResponseMessage resp = Request.CreateResponse((HttpStatusCode)qry[0], qry[1]);
                    return resp;
                }
            }
            catch (Exception e)
            {
                //Action failure, attempt to send email
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return Request.CreateResponse(HttpStatusCode.BadRequest, errorMessages);
                    Dictionary<String, Object> request = new Dictionary<String, Object>();
                    request.Add("Source", "Telephone API");
                    request.Add("Request Uri", Request.RequestUri);
                    request.Add("Methods", Request.Method);
                    request.Add("Headers", Request.Headers);
                    request.Add("Content", Request.Content);
                    request.Add("Parameters", new List<object>() { s, TelephoneNumber });
                    maxtime_functions.sendEmail("get_telephone_details failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(request));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessages);
            }
        }


        [HttpPost]
        public HttpResponseMessage get_employee_details([FromBody]String Employee_Code)
        {
            HttpStatusCode hsc = new HttpStatusCode();
            IEnumerable<String> s = Request.Headers.GetValues("session_token");
            IEnumerable<String> app_name = !Request.Headers.Any(ob => ob.Key == "app_name") || Request.Headers.GetValues("app_name") == null ? null : Request.Headers.GetValues("app_name");
            IEnumerable<String> app_major = !Request.Headers.Any(ob => ob.Key == "app_major") || Request.Headers.GetValues("app_major") == null ? null : Request.Headers.GetValues("app_major");
            IEnumerable<String> app_minor = !Request.Headers.Any(ob => ob.Key == "app_minor") || Request.Headers.GetValues("app_minor") == null ? null : Request.Headers.GetValues("app_minor");

            try
            {
                ConnectionRepository cr = new ConnectionRepository();
                Dictionary<String, Object> prms = new Dictionary<String, Object>();

                using (HttpClient hc = new HttpClient())
                {
                    //No Serial Number provided
                    if (Employee_Code == null || Employee_Code == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Serial Number Provided");
                    prms.Add("Employee_Code", Employee_Code);

                    //No session_token provided, return error response.
                    if (s == null || s.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Session Token Provided");
                    if (app_name == null || app_name.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Name Provided");
                    if (app_major == null || app_major.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Major Version Provided");
                    if (app_minor == null || app_minor.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Minor Version Provided");

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"session/check_valid_new");
                    req.Headers.Add("session_token", s.First());
                    req.Headers.Add("app_name", app_name.First());
                    req.Headers.Add("app_major", app_major.First());
                    req.Headers.Add("app_minor", app_minor.First());
                    req.Headers.Add("api_name", ConnectionRepository.API_name);
                    req.Headers.Add("api_major", ConnectionRepository.API_major);
                    req.Headers.Add("api_minor", ConnectionRepository.API_minor);

                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to check valid session for get_employee_details", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, Boolean>> b = JsonConvert.DeserializeObject<List<Dictionary<String, Boolean>>>(result.Content.ReadAsStringAsync().Result);

                    //Invalid, return error response.
                    if (b == null || b[0].First().Value == false) return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Session Token Provided");

                    req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_connection");
                    req.Headers.Add("session_token", s.First());

                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to get the connection for get_employee_details", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    String connection = cr.get_connection(d);
                    MySqlConnection msc = new MySqlConnection();
                    if (connection == "Invalid Connection")
                    {
                        throw new Exception("Failed to get the connection for get_employee_details", new Exception(connection));
                    }
                    else
                    {
                        msc = new MySqlConnection(connection);
                    }

                    List<Object> qry = cr.createQuery(msc, @"select Forename, Surname from employees where code = @Employee_Code;", prms);
                    HttpResponseMessage resp = Request.CreateResponse((HttpStatusCode)qry[0], qry[1]);
                    return resp;
                }
            }
            catch (Exception e)
            {
                //Action failure, attempt to send email
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return Request.CreateResponse(HttpStatusCode.BadRequest, errorMessages);
                    Dictionary<String, Object> request = new Dictionary<String, Object>();
                    request.Add("Source", "Telephone API");
                    request.Add("Request Uri", Request.RequestUri);
                    request.Add("Methods", Request.Method);
                    request.Add("Headers", Request.Headers);
                    request.Add("Content", Request.Content);
                    request.Add("Parameters", new List<object>() { s, Employee_Code });
                    maxtime_functions.sendEmail("get_employee_details failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(request));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessages);
            }
        }

        [HttpPost]
        public HttpResponseMessage post_punch([FromBody]Dictionary<String,String> oValues)
        {
            HttpStatusCode hsc = new HttpStatusCode();
            IEnumerable<String> s = Request.Headers.GetValues("session_token");
            IEnumerable<String> app_name = !Request.Headers.Any(ob => ob.Key == "app_name") || Request.Headers.GetValues("app_name") == null ? null : Request.Headers.GetValues("app_name");
            IEnumerable<String> app_major = !Request.Headers.Any(ob => ob.Key == "app_major") || Request.Headers.GetValues("app_major") == null ? null : Request.Headers.GetValues("app_major");
            IEnumerable<String> app_minor = !Request.Headers.Any(ob => ob.Key == "app_minor") || Request.Headers.GetValues("app_minor") == null ? null : Request.Headers.GetValues("app_minor");

            try
            {
                ConnectionRepository cr = new ConnectionRepository();
                Dictionary<String, Object> prms = new Dictionary<String, Object>();
                prms.Add("Employee_Code", oValues["employee_code"].ToString());
                prms.Add("TelephoneNumber", oValues["telephonenumber"].ToString());
                prms.Add("PunchTime", oValues["punchtime"].ToString());

                using (HttpClient hc = new HttpClient())
                {

                    //No session_token provided, return error response.
                    if (s == null || s.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Session Token Provided");
                    if (app_name == null || app_name.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Name Provided");
                    if (app_major == null || app_major.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Major Version Provided");
                    if (app_minor == null || app_minor.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Minor Version Provided");

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"session/check_valid_new");
                    req.Headers.Add("session_token", s.First());
                    req.Headers.Add("app_name", app_name.First());
                    req.Headers.Add("app_major", app_major.First());
                    req.Headers.Add("app_minor", app_minor.First());
                    req.Headers.Add("api_name", ConnectionRepository.API_name);
                    req.Headers.Add("api_major", ConnectionRepository.API_major);
                    req.Headers.Add("api_minor", ConnectionRepository.API_minor);

                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to check valid session for creating a clock op", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, Boolean>> b = JsonConvert.DeserializeObject<List<Dictionary<String, Boolean>>>(result.Content.ReadAsStringAsync().Result);

                    //Invalid, return error response.
                    if (b == null || b[0].First().Value == false) return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Session Token Provided");

                    req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_connection");
                    req.Headers.Add("session_token", s.First());

                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to get the connection for creating a clock op", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    String connection = cr.get_connection(d);
                    MySqlConnection msc = new MySqlConnection();
                    if (connection == "Invalid Connection")
                    {
                        throw new Exception("Failed to get the connection for the session when creating a punch", new Exception(connection));
                    }
                    else
                    {
                        msc = new MySqlConnection(connection);
                    }

                    List<Object> qry = cr.createCommand(
                            msc,
                            @"
                                INSERT INTO PUNCHES_RAW SET 
                                    Clock=(SELECT Code FROM organiz WHERE tel = @TelephoneNumber AND CanTelClockIn = 1 LIMIT 1), 
                                    Employee = @Employee_Code,
                                    PunchDT = @PunchTime,
                                    ID_Method = 'T',
                                    ID_Data = @TelephoneNumber;
                            ",
                            prms
                        );
                    HttpResponseMessage resp = Request.CreateResponse((HttpStatusCode)qry[0], qry[1]);
                    return resp;
                }
            }
            catch (Exception e)
            {
                //Action failure, attempt to send email
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return Request.CreateResponse(HttpStatusCode.BadRequest, errorMessages);
                    Dictionary<String, Object> request = new Dictionary<String, Object>();
                    request.Add("Source", "Push API");
                    request.Add("Request Uri", Request.RequestUri);
                    request.Add("Methods", Request.Method);
                    request.Add("Headers", Request.Headers);
                    request.Add("Content", Request.Content);
                    request.Add("Parameters", new List<object>() { s, oValues });
                    maxtime_functions.sendEmail("create failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(request));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessages);
            }
        }

        [HttpPost]
        public HttpResponseMessage process_punches([FromBody]String Employee_Code)
        {
            HttpStatusCode hsc = new HttpStatusCode();
            IEnumerable<String> s = Request.Headers.GetValues("session_token");
            IEnumerable<String> app_name = !Request.Headers.Any(ob => ob.Key == "app_name") || Request.Headers.GetValues("app_name") == null ? null : Request.Headers.GetValues("app_name");
            IEnumerable<String> app_major = !Request.Headers.Any(ob => ob.Key == "app_major") || Request.Headers.GetValues("app_major") == null ? null : Request.Headers.GetValues("app_major");
            IEnumerable<String> app_minor = !Request.Headers.Any(ob => ob.Key == "app_minor") || Request.Headers.GetValues("app_minor") == null ? null : Request.Headers.GetValues("app_minor");
            bool rb = false;
            IntPtr mDLL = IntPtr.Zero;

            try
            {
                ConnectionRepository cr = new ConnectionRepository();
                Dictionary<String, Object> prms = new Dictionary<String, Object>();

                using (HttpClient hc = new HttpClient())
                {

                    //No session_token provided, return error response.
                    if (s == null || s.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No Session Token Provided");
                    if (app_name == null || app_name.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Name Provided");
                    if (app_major == null || app_major.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Major Version Provided");
                    if (app_minor == null || app_minor.First() == "") return Request.CreateResponse(HttpStatusCode.BadRequest, "No App Minor Version Provided");

                    prms.Add("employee", Employee_Code);

                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"session/check_valid_new");
                    req.Headers.Add("session_token", s.First());
                    req.Headers.Add("app_name", app_name.First());
                    req.Headers.Add("app_major", app_major.First());
                    req.Headers.Add("app_minor", app_minor.First());
                    req.Headers.Add("api_name", ConnectionRepository.API_name);
                    req.Headers.Add("api_major", ConnectionRepository.API_major);
                    req.Headers.Add("api_minor", ConnectionRepository.API_minor);

                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to check valid session for process_punches ", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, Boolean>> b = JsonConvert.DeserializeObject<List<Dictionary<String, Boolean>>>(result.Content.ReadAsStringAsync().Result);

                    //Invalid, return error response.
                    if (b == null || b[0].First().Value == false) return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Session Token Provided");

                    req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_connection");
                    req.Headers.Add("session_token", s.First());

                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to get the connection for processing punches", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    String connection = cr.get_connection(d);
                    MySqlConnection msc = new MySqlConnection();
                    if (connection == "Invalid Connection")
                    {
                        throw new Exception("Failed to get the connection for the session when processing punches", new Exception(connection));
                    }
                    else
                    {
                        msc = new MySqlConnection(connection);
                    }

                    req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_database_version");
                    req.Headers.Add("session_token", s.First());

                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (result.Content.ReadAsStringAsync().Result.Contains("No result found")) return Request.CreateResponse(HttpStatusCode.OK, "No active DLL found, this database requires a server.");
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to get the database version for processing punches", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d2 = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    List<Object> qry = cr.createQuery
                        (
                            msc,
                            @"
                            select 
                                UID, 
                                Clock,
                                Clock AS Site,
                                Employee,
                                PunchDT,
                                ID_Method,
                                Longitude,
                                Latitude
                                from punches_raw
                                where isprocessed = false and Employee = @employee and ID_Method = 'T'
                                order by punchdt
                                limit 10000;
                            ",
                            prms
                        );

                    if ((HttpStatusCode)qry[0] == HttpStatusCode.InternalServerError) throw new Exception("Failed to get list of unprocessed punches");
                    if ((HttpStatusCode)qry[0] == HttpStatusCode.OK)
                    {
                        DataTable punchList = (DataTable)qry[1];
                        List<Dictionary<String, Object>> punchesToProcess = new List<Dictionary<String, Object>>();
                        foreach (DataRow dr in punchList.Rows)
                        {
                            Dictionary<String, Object> temp = new Dictionary<String, Object>();
                            temp.Add("UID", dr.ItemArray[0].ToString());
                            temp.Add("Clock", dr.ItemArray[1].ToString());
                            temp.Add("Site", dr.ItemArray[2].ToString());
                            temp.Add("Employee", dr.ItemArray[3].ToString());
                            temp.Add("PunchDT", Convert.ToDateTime(dr.ItemArray[4]).ToString("yyyy-MM-dd HH:mm:ss"));
                            temp.Add("ID_Method", dr.ItemArray[5].ToString());
                            temp.Add("Longitude", dr.ItemArray[6].ToString());
                            temp.Add("Latitude", dr.ItemArray[7].ToString());
                            punchesToProcess.Add(temp);
                        }

                        for (int i = 0; i < punchesToProcess.Count; i++)
                        {


                            //insert into punches set
                            //Site = @Site, Employee = @Employee, PunchDateTime = @PunchDT, ForDate = @outForDate, DayAssignment = @outUID_Shift,
                            //ID_Method = @ID_Method, Longitude = @Longitude, Latitude = @Latitude;

                            qry = cr.createQuery
                            (
                                msc,
                                @"
                            CALL MaxTIME_CalcShiftDetailsForPunch(@Employee,@PunchDT,@outForDate,@outUID_Shift);

                            insert into punches (Site,Employee,PunchDateTime,ForDate,DayAssignment,ID_Method,Longitude,Latitude)
                            VALUE (@Site,@Employee,@PunchDT,@outForDate,@outUID_Shift,@ID_Method,@Longitude,@Latitude)
                            on duplicate key update PunchDateTime = @PunchDT;

                            select DATE_FORMAT(@outForDate,'%d %m %Y') AS ForDate;
                            ",
                                punchesToProcess[i]
                            );

                            if ((HttpStatusCode)qry[0] == HttpStatusCode.OK && d2[0]["enabled"] == "True")
                            {
                                DataTable dt = (DataTable)qry[1];
                                String dllPath = HttpContext.Current.Server.MapPath("~/DLLS/" + d2[0]["dll_name"]);
                                if (File.Exists(dllPath))
                                {
                                    mDLL = NativeMethods.LoadLibrary(dllPath);
                                    if (mDLL == IntPtr.Zero) throw new Exception("Failed to set the DLL");

                                    IntPtr pAddressOfFunctionToCall = NativeMethods.GetProcAddress(mDLL, "MAXTIME_Calc");
                                    if (mDLL == IntPtr.Zero) throw new Exception("Failed to set proc address for DLL");

                                    MAXTIME_Calc maxtime_Calc = (MAXTIME_Calc)Marshal.GetDelegateForFunctionPointer(pAddressOfFunctionToCall, typeof(MAXTIME_Calc));
                                    StringBuilder errorString = new StringBuilder(4096);
                                    int DelphiDate = (int)Math.Truncate(Convert.ToDateTime(dt.Rows[0].ItemArray[0]).ToOADate());
                                    //int dllResult = MAXTIME_Calc(d[0]["server"], Convert.ToInt32(d[0]["port"]), d[0]["username"], d[0]["password"],d[0]["name"], Convert.ToInt32(prms["Employee"]), DelphiDate, out errorString);

                                    int dllResult = maxtime_Calc(d[0]["server"], Convert.ToInt32(d[0]["port"]), d[0]["username"], d[0]["password"], d[0]["name"], Convert.ToInt32(punchesToProcess[i]["Employee"]), DelphiDate, out errorString);
                                    rb = NativeMethods.FreeLibrary(mDLL);

                                    errorString = errorString == null ? new StringBuilder(4096) : errorString;
                                    if (!String.IsNullOrEmpty(errorString.ToString())) throw new Exception("maxtime_Calc threw an exception", new Exception(errorString.ToString()));


                                    if (dllResult == 0)
                                    {
                                        qry = cr.createCommand
                                        (
                                            msc,
                                            @"
                                                UPDATE punches_raw set IsProcessed = 1 where uid = @UID;
                                            ",
                                            punchesToProcess[i]
                                        );
                                    }
                                }
                            }
                        }
                    }

                    if ((HttpStatusCode)qry[0] == HttpStatusCode.OK)
                    {
                        HttpResponseMessage resp = Request.CreateResponse((HttpStatusCode)qry[0], qry[1]);
                        return resp;
                    }
                    else
                    {
                        HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.OK, qry[1]);
                        return resp;
                    }
                }
            }
            catch (Exception e)
            {
                //Need to free memory if exception occurs.
                if (mDLL != IntPtr.Zero) rb = NativeMethods.FreeLibrary(mDLL);
                //Action failure, attempt to send email
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return Request.CreateResponse(HttpStatusCode.BadRequest, errorMessages);
                    Dictionary<String, Object> request = new Dictionary<String, Object>();
                    request.Add("Source", "Push API");
                    request.Add("Request Uri", Request.RequestUri);
                    request.Add("Methods", Request.Method);
                    request.Add("Headers", Request.Headers);
                    request.Add("Content", Request.Content);
                    request.Add("Parameters", new List<object>() { s });
                    maxtime_functions.sendEmail("process_punches failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(request));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessages);
            }
        }
    }
}
