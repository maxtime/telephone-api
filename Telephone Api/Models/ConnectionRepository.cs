﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace Telephone_Api.Models
{
    public class ConnectionRepository
    {
        public const String URL = @"http://auth.maxtime.co.uk/v1.3/";
   //     public const String URL = @"http://localhost:51747/";
        public const String error_email = "development@ldssystems.co.uk";
        public const String error_pass = "$sonic47235!";
        public const String API_name = "telephone api";
        public const String API_major = "1";
        public const String API_minor = "0";

        public List<Object> createQuery(MySqlConnection msc, String qry, Dictionary<String, object> prms = null, Boolean byteConvert = true)
        {
            List<Object> response = new List<Object>();
            DataTable dt = new DataTable();
            try
            {
                using (msc)
                {
                    msc.Open();
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = msc;
                        cmd.CommandText = qry;

                        if (prms != null)
                        {
                            foreach (KeyValuePair<String, object> kvp in prms)
                            {
                                cmd.Parameters.Add(new MySqlParameter(kvp.Key, kvp.Value));
                            }
                        }

                        using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                        {
                            da.Fill(dt);
                            da.Dispose();

                            if (byteConvert)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    for (int j = 0; j < dt.Rows[i].Table.Columns.Count; j++)
                                    {
                                        String s = null;
                                        try
                                        {
                                            s = dt.Rows[i].Field<object>(j).GetType().ToString();
                                        }
                                        catch
                                        {
                                            s = null;
                                        }
                                        if (s == "System.Byte[]")
                                        {
                                            DataTable dtclone = dt.Clone();
                                            dtclone.Columns[j].DataType = typeof(String);
                                            foreach (DataRow dr in dt.Rows)
                                            {
                                                dtclone.ImportRow(dr);
                                            }
                                            dtclone.Rows[i][j] = Encoding.ASCII.GetString(dt.Rows[i].Field<byte[]>(j));
                                            dt = dtclone;
                                        }
                                    }
                                }
                            }

                            if (dt.Rows.Count == 0)
                            {
                                cmd.Dispose();
                                msc.Close();
                                msc.Dispose();
                                response.Add(HttpStatusCode.BadRequest);
                                response.Add(new Dictionary<String, String>() { { "Error:", "No result found, are you passing through valid parameters?" } });
                            }
                        }
                        cmd.Dispose();
                    }
                    msc.Close();
                    msc.Dispose();
                }
                response.Add(HttpStatusCode.OK);
                response.Add(dt);
            }
            catch (Exception e)
            {
                //Action failure, attempt to send email
                msc.Dispose();
                msc.Close();
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    Dictionary<String, Object> properties = new Dictionary<String, Object>();
                    properties.Add("Source", "Telephone API");
                    properties.Add("Database", msc.Database);
                    properties.Add("Query", qry);
                    properties.Add("Parameters", prms);
                    maxtime_functions.sendEmail("Query failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(properties));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                response.Add(HttpStatusCode.InternalServerError);
                response.Add(errorMessages);
            }

            finally
            {
                msc.Close();
                msc.Dispose();
                MySqlConnection.ClearPool(msc);
            }
            return response;
        }

        public List<Object> createCommand(MySqlConnection msc, String qry, Dictionary<String, object> prms = null)
        {
            List<Object> response = new List<Object>();
            int rowsAffected = 0;
            try
            {
                using (msc)
                {
                    msc.Open();
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = msc;
                        cmd.CommandText = qry;

                        if (prms != null)
                        {
                            foreach (KeyValuePair<String, object> kvp in prms)
                            {
                                if (kvp.Value != null && kvp.Value.GetType() == typeof(byte[]))
                                {
                                    cmd.Parameters.Add(new MySqlParameter(kvp.Key, MySqlDbType.Blob));
                                    cmd.Parameters[kvp.Key].Value = kvp.Value;
                                }
                                else if (kvp.Value == null)
                                {
                                    cmd.Parameters.Add(kvp.Key, MySqlDbType.VarChar);
                                    cmd.Parameters[kvp.Key].Value = DBNull.Value;
                                }
                                else
                                {
                                    cmd.Parameters.Add(new MySqlParameter(kvp.Key, kvp.Value));
                                }
                            }
                        }

                        using (MySqlDataReader rdr = cmd.ExecuteReader())
                        {
                            rowsAffected = rdr.RecordsAffected;

                            rdr.Close();
                            rdr.Dispose();
                        }
                        cmd.Dispose();
                    }
                    msc.Close();
                    msc.Dispose();
                }
                response.Add(HttpStatusCode.OK);
                response.Add(rowsAffected);
            }
            catch (Exception e)
            {
                //Ignore duplicate punches
                if (e.Message.Contains("Duplicate"))
                {
                    response.Add(HttpStatusCode.OK);
                    response.Add(1);
                }
                else
                {
                    //Action failure, attempt to send email
                    msc.Dispose();
                    msc.Close();
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        Dictionary<String, Object> properties = new Dictionary<String, Object>();
                        properties.Add("Source", "Telephone API");
                        properties.Add("Database", msc.Database);
                        properties.Add("Query", qry);
                        properties.Add("Parameters", prms);
                        maxtime_functions.sendEmail("Query Command failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(properties));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                    response.Add(HttpStatusCode.InternalServerError);
                    response.Add(errorMessages);
                }
            }
            finally
            {
                msc.Close();
                msc.Dispose();
                MySqlConnection.ClearPool(msc);
            }
            return response;
        }

        public String get_connection(List<Dictionary<String, String>> d)
        {
            try
            {
                //    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, ConnectionRepository.URL + @"connection/get_connection");
                //    req.Headers.Add("session_token", session_token);

                //    HttpResponseMessage result = hc.SendAsync(req).Result;
                //    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                //Parameter(s) missing, invalid connection
                if (d == null || d.Count == 0 || !d[0].ContainsKey("server") || !d[0].ContainsKey("port") || !d[0].ContainsKey("username") || !d[0].ContainsKey("password") || !d[0].ContainsKey("name"))
                {
                    return "Invalid Connection";
                }

                return "server=" + d[0]["server"] + ";port=" + d[0]["port"] + ";User Id=" + d[0]["username"] + ";password=" + d[0]["password"] + ";database=" + d[0]["name"] + ";" + d[0]["additional"];
            }
            catch (Exception e)
            {
                //Action failure, attempt to send email
                String errorMessages = maxtime_functions.GetExceptionMessages(e);
                try
                {
                    Dictionary<String, Object> properties = new Dictionary<String, Object>();
                    properties.Add("Source", "Telephone API");
                    properties.Add("Parameters", d);
                    maxtime_functions.sendEmail("get_connection request to Auth Api failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(properties));
                }
                catch (Exception ex)
                {
                    //Failed to send email.
                }
                return "Invalid Connection";
            }

        }
    }
}