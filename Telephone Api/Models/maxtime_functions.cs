﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Telephone_Api.Models
{
    public static class maxtime_functions
    {
        public static void sendEmail(String Subject, String Body)
        {
            MailMessage mail = new MailMessage();

            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(ConnectionRepository.error_email, ConnectionRepository.error_pass);
            smtp.Port = 587;
            smtp.EnableSsl = true;

            mail.From = new MailAddress(ConnectionRepository.error_email, "Telephone Api");
            mail.To.Add("lee.thomas@ldssystems.co.uk");
            mail.To.Add("chad@maxtime.co.uk");
            mail.Subject = Subject;
            mail.Body = Body;

            smtp.Send(mail);
        }

        public static String GetExceptionMessages(this Exception e, String messages = "")
        {
            if (e == null) return String.Empty;
            if (messages == "") messages = e.Message;
            if (e.InnerException != null)
            {
                messages += Environment.NewLine + "Inner Exception: " + GetExceptionMessages(e.InnerException);
            }
            return messages;
        }
    }
}